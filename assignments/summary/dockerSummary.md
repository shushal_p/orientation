<h3>Docker overview</h3>

Docker is an open platform for developing, shipping, and running applications. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly. With Docker, you can manage your infrastructure in the same ways you manage your applications. By taking advantage of Docker’s methodologies for shipping, testing, and deploying code quickly, you can significantly reduce the delay between writing code and running it in production.

## Why docker?
    It is very hard to manage dependencies as we build large projects and if we want to deploy them into various environments.Thus, we are using docker to deploy our product.For this you don't need to master docker but need to know the basics of docker.

<h3>The Docker platform</h3>
Docker provides the ability to package and run an application in a loosely isolated environment called a container. The isolation and security allow you to run many containers simultaneously on a given host. Containers are lightweight because they don’t need the extra load of a hypervisor, but run directly within the host machine’s kernel. This means you can run more containers on a given hardware combination than if you were using virtual machines. You can even run Docker containers within host machines that are actually virtual machines!

Docker provides tooling and a platform to manage the lifecycle of your containers:
1. Develop your application and its supporting components using containers.
2. The container becomes the unit for distributing and testing your application.
3. When you’re ready, deploy your application into your production environment, as a container or an orchestrated service. This works the same whether your production environment is a local data center, a cloud provider, or a hybrid of the two.

<h3>Docker Engine</h3>
Docker Engine is a client-server application with these major components: <br> 
1. A server which is a type of long-running program called a daemon process (the dockerd command). <br>
2. A REST API which specifies interfaces that programs can use to talk to the daemon and instruct it what to do. <br>
3. A command line interface (CLI) client (the docker command). <br>

![Engine Components Flow](/extras/engine.png)

The CLI uses the Docker REST API to control or interact with the Docker daemon through scripting or direct CLI commands. Many other Docker applications use the underlying API and CLI. The daemon creates and manages Docker objects, such as images, containers, networks, and volumes.

What can I use Docker for?
Fast, consistent delivery of your applications

Docker streamlines the development lifecycle by allowing developers to work in standardized environments using local containers which provide your applications and services. Containers are great for continuous integration and continuous delivery (CI/CD) workflows.

Consider the following example scenario:

Your developers write code locally and share their work with their colleagues using Docker containers.
They use Docker to push their applications into a test environment and execute automated and manual tests.
When developers find bugs, they can fix them in the development environment and redeploy them to the test environment for testing and validation.
When testing is complete, getting the fix to the customer is as simple as pushing the updated image to the production environment.
Responsive deployment and scaling

Docker’s container-based platform allows for highly portable workloads. Docker containers can run on a developer’s local laptop, on physical or virtual machines in a data center, on cloud providers, or in a mixture of environments.

Docker’s portability and lightweight nature also make it easy to dynamically manage workloads, scaling up or tearing down applications and services as business needs dictate, in near real time.

<h3>Running more workloads on the same hardware</h3>

Docker is lightweight and fast. It provides a viable, cost-effective alternative to hypervisor-based virtual machines, so you can use more of your compute capacity to achieve your business goals. Docker is perfect for high density environments and for small and medium deployments where you need to do more with fewer resources.

<h3>Docker architecture</h3>
Docker uses a client-server architecture. The Docker client talks to the Docker daemon, which does the heavy lifting of building, running, and distributing your Docker containers. The Docker client and daemon can run on the same system, or you can connect a Docker client to a remote Docker daemon. The Docker client and daemon communicate using a REST API, over UNIX sockets or a network interface.

The Docker daemon
The Docker daemon (dockerd) listens for Docker API requests and manages Docker objects such as images, containers, networks, and volumes. A daemon can also communicate with other daemons to manage Docker services.

The Docker client
The Docker client (docker) is the primary way that many Docker users interact with Docker. When you use commands such as docker run, the client sends these commands to dockerd, which carries them out. The docker command uses the Docker API. The Docker client can communicate with more than one daemon.

Docker registries
A Docker registry stores Docker images. Docker Hub is a public registry that anyone can use, and Docker is configured to look for images on Docker Hub by default. You can even run your own private registry.

When you use the docker pull or docker run commands, the required images are pulled from your configured registry. When you use the docker push command, your image is pushed to your configured registry.

<h4>Docker objects</h4>
When you use Docker, you are creating and using images, containers, networks, volumes, plugins, and other objects. This section is a brief overview of some of those objects.

<h4>IMAGES</h4>
An image is a read-only template with instructions for creating a Docker container. Often, an image is based on another image, with some additional customization. For example, you may build an image which is based on the ubuntu image, but installs the Apache web server and your application, as well as the configuration details needed to make your application run.

You might create your own images or you might only use those created by others and published in a registry. To build your own image, you create a Dockerfile with a simple syntax for defining the steps needed to create the image and run it. Each instruction in a Dockerfile creates a layer in the image. When you change the Dockerfile and rebuild the image, only those layers which have changed are rebuilt. This is part of what makes images so lightweight, small, and fast, when compared to other virtualization technologies.

<h4>CONTAINERS</h4>
A container is a runnable instance of an image. You can create, start, stop, move, or delete a container using the Docker API or CLI. You can connect a container to one or more networks, attach storage to it, or even create a new image based on its current state.

By default, a container is relatively well isolated from other containers and its host machine. You can control how isolated a container’s network, storage, or other underlying subsystems are from other containers or from the host machine.

A container is defined by its image as well as any configuration options you provide to it when you create or start it. When a container is removed, any changes to its state that are not stored in persistent storage disappear.

## Docker Terminologies

1. **Docker**
- It is just a code for developers to develop and to run applications with containers.
2. **Docker Image**
- It contains everything needed to run an applications as a container.
- code
- runtime
- libraries
- environment variables
- configuration files

3. **Container**
- It is nothing but a running Docker image.
- You can create multiple containers from one image.
4. **Docker HUb**
- It is just like GitHub but for docker images and containers.

### Common Operations on Dockers
Typically, you will be using docker in the given flow.

1. Download/pull the docker images that you want to work with.
2.   Copy your code inside the docker
3. Access docker terminal
4. Install and additional required dependencies
5. Compile and Run the Code inside docker
6. Document steps to run your program in README.md file
7. Commit the changes done to the docker.
8. Push docker image to the docker-hub and share repository with people who want to try your code.

#### View all containers running on Docker's host

`docker ps`

#### Start any stopped containers

`docker start <container-name or container-id>`

#### Stop any running containers

`docker stop <container-name or beginning-of-container-id>` 

#### Create containers from Docker Images

`docker run <container-name>`

#### Delete a container

`docker rm <container-name>`

#### Download/pull Docker Images

`docker pull <image-author>/<image-name>`

#### Run Docker Image as a bash script

`docker run -ti <image-author>/<image-name> /bin/bash`

#### Copy any file inside Docker container with <container-id>

`docker cp <code-filename> <container-id>:/`

Furthermore, write a bash script say `install-dependencies.sh` to install all dependencies for successful execution of <code-filename> 

For instance, say <code-filename> was a basic python `.py` executable

`install-dependencies.sh` will include:

	apt update
	apt install python3

Now, copy the bash script into the same Docker container too

`docker cp install-dependencies.sh <container-id>:/`

#### Installing dependencies

 - Allow running bash script as executable 
 
 `docker exec -it <container-id> chmod +x install-dependencies.sh`
	
 - Install dependencies
 
 `docker exec -it <container-id> /bin/bash ./install-dependencies.sh`
	
#### Run program inside the container

Start the container

`docker start <container-id>`

For the example given above, the following will be the line for execution

`docker exec <container-id> python3 <code-filename>`

#### Save copied program inside Docker Image

`docker commit <container-id> <image-author>/<image-name>`

#### Tag Docker Image with a different name

`docker tag <image-author>/<image-name> <user-name>/<repo-name>`

#### Push Docker Image into DockerHub

`docker push <user-name>/<repo-name>`




